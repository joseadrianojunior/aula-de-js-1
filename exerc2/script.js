//intervalos
let entrada = 1000
if(entrada >= 0 && entrada <= 25){
    console.log("O número está dentro do intervalo [0,25]")
}
else if(entrada > 25 && entrada <= 50){
    console.log("O número está dentro do intervalo (25,50]")
}
else if(entrada > 50 && entrada <= 75){
    console.log("O número está dentro do intervalo (50,75]")
}
else if(entrada > 75 && entrada <= 100){
    console.log("O número está dentro do intervalo (75,100]")
}
else{
    console.log("Fora de intervalo")
}